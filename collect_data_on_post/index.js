var advisor = require('azure-arm-advisor');
var compute = require('azure-arm-compute');
var network = require('azure-arm-network');
var storage = require('azure-arm-storage');
var resource = require('azure-arm-resource');
var msrest = require('ms-rest-azure');

//Authentication
function getCredentials() {
    return new Promise(function(resolve, reject) {
        msrest.loginWithServicePrincipalSecret(
            process.env['AZURE_CLIENTID'], 
            process.env['AZURE_SECRET'],
            process.env['AZURE_TENANTID'],
            (err, credentials) => {
                if (err) {
                    context.log("Got Error");
                    reject(err);
                } else {
                    context.log("Got Creds");
                    resolve(credentials);
                }
            }
        );
    });
}

module.exports = function (context, req) {
    var statusCode = 400;
    var responseBody = "Invalid request object";

    if (typeof req.body != 'undefined' && typeof req.body == 'object') {
        //context.log(req.body);
        if(req.body.company && req.body.sostenuto_id) {
            context.log("Company: " + req.body.company);
            context.log("Sostenuto Id: " + req.body.sostenuto_id);

            //Action Code
            var credentials = getCredentials();
            credentails.then(function(result) {
                var subClient = new resource.SubscriptionClient(result);
                    subClient.subscriptions.list().then(data => {
                    context.log(data);
                }, function(err) {
                    console.log(err);
                });
            });
            
            statusCode = 201;
            context.bindings.outTable = req.body;
            responseBody = "Success";
        } else {
            statusCode = 400;
            responseBody = "Failed Bad Request";
        }
    }

    context.res = {
        status: statusCode,
        body: responseBody
    };

    context.done();
};